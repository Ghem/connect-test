var connect = require("connect");
var url = require("url");
var app = connect();
var path = require("path");
var mime = require("mime");
var fs = require("fs");

var homepagelocation = __dirname + "/html/home.html"
var somejson = require(__dirname + "/some-json.json");
var sometext = "this is some plain text";
//var somefile = __dirname + "/files/img.jpg";
var somefile = __dirname + "/files/video.mp4"
var error404json = require(__dirname + "/404.json");

function homePage(req, res, next){
  res.setHeader("Content-Type", "text/html" );
  fs.readFile(homepagelocation, function(err, html){
    if (err) {
      throw err;
    }
    res.write(html);
    res.end();
  })
}

function loggingMiddleware(req, res, next) {
  console.log("--------------------------------------");
  console.log("The request method is: " + req.method);
  console.log("The request url is: " + req.url);
  var queryData = url.parse(req.url, true).query;
  console.log("The query parameters are: " + queryData.name);
  next();
}

function pageJSON(req, res, next) {
  res.setHeader("Content-type", "application/json");
  res.end(JSON.stringify(somejson));  
  next();
}

function pageText(req, res, next) {
  res.setHeader("Content-type", "text/plain");
  res.end(sometext);
  next();
}

function pageFile(req, res, next) {
  var filename = path.basename(somefile);
  var mimetype = mime.getType(somefile);

  res.setHeader("Content-disposition", "attachment; filename=" + filename);
  res.setHeader("Content-type", mimetype);

  var filestream = fs.createReadStream(somefile);
  filestream.pipe(res);

  filestream.on("finish", function(){
    res.send();
    next();
  })
}

function page404(req, res, next) {
  next(new Error());
}

function page500(req, res, next) {
  next(new Error(500));
}

function onerror(err, req, res, next) {
  switch (req.url){
    case "/404":
      res.statusCode = 404;
      res.setHeader("Content-type", "application/json");
      res.end(JSON.stringify(error404json));  
      break;
      
    case "/500":
      res.statusCode = 500;
      res.setHeader("Content-type", "text/plain");
      res.end('500');
      break;
    
    default:
      res.writeHead(301, { Location: "/" });
      res.end();
  }
}

app
  .use(loggingMiddleware)
  .use("/json", pageJSON)
  .use("/text", pageText)
  .use("/file", pageFile)
  .use("/404", page404)
  .use("/500", page500)
  .use("/", homePage)
  .use(onerror)
  .listen(3031);

console.log("Server is listening");
