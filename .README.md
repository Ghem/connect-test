Install dependencies

``` bash
npm i
```

Run at localhost:3031

``` bash
npm run dev
```